
#include <stdint.h>

// returns the square of the passed value (n^2)
double square(double val);

// returns the factorial of the passed value (n!)
uint64_t factorial(int n);